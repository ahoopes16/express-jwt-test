const ROLES = {
    MEMBER: 'member',
    ADMIN: 'admin',
};

let users = [
    {
        username: 'john',
        password: 'password123admin',
        role: ROLES.ADMIN,
    },
    {
        username: 'anna',
        password: 'password123member',
        role: ROLES.MEMBER,
    },
];

let books = [
    {
        author: 'Chinua Achebe',
        country: 'Nigeria',
        language: 'English',
        pages: 209,
        title: 'Things Fall Apart',
        year: 1958,
    },
    {
        author: 'Hans Christian Andersen',
        country: 'Denmark',
        language: 'Danish',
        pages: 784,
        title: 'Fairy tales',
        year: 1836,
    },
    {
        author: 'Dante Alighieri',
        country: 'Italy',
        language: 'Italian',
        pages: 928,
        title: 'The Divine Comedy',
        year: 1315,
    },
];

let refreshTokens = [];

module.exports = {
    books,
    ROLES,
    users,
    refreshTokens,
};
