const express = require('express');
const bodyParser = require('body-parser');

const { authenticateJWT, allowAdminOnly } = require('./middleware');
const { books } = require('./database');

const app = express();
app.use(bodyParser.json());

app.get('/books', authenticateJWT, (req, res) => {
    res.json(books);
});

app.post('/books', authenticateJWT, allowAdminOnly, (req, res) => {
    const book = req.body;
    const { author, country, language, pages, title, year } = book;

    if (!(author && country && language && pages && title && year)) {
        return res
            .status(400)
            .send(
                'The following fields are required: author, country, language, pages, title, year'
            );
    }

    books.push(book);
    res.send('Book added successfully!');
});

app.listen(4000, () => {
    console.log('Books service started on port 4000!');
});
