// Dependencies
const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

// Constants
let { users, refreshTokens } = require('./database');
const {
    accessTokenSecret,
    accessTokenExpireTime,
    refreshTokenSecret,
} = require('./env');

const app = express();

app.use(bodyParser.json());

app.post('/login', (req, res) => {
    // Read username and password from request body
    // Ideally these would be encrypted with a shared secret between the app and the server
    const { username, password } = req.body;

    // Find user by username and password
    const user = users.find(
        u => u.username === username && u.password === password
    );

    if (!user) {
        return res.status(400).send('Username or password is incorrect');
    }

    const accessToken = jwt.sign(
        { username: user.username, role: user.role },
        accessTokenSecret,
        { expiresIn: accessTokenExpireTime }
    );
    const refreshToken = jwt.sign(
        { username: user.username, role: user.role },
        refreshTokenSecret
    );

    refreshTokens.push(refreshToken);

    res.json({ accessToken, refreshToken });
});

app.post('/token', (req, res) => {
    const { refreshToken } = req.body;

    if (!refreshToken) {
        return res.sendStatus(401);
    }

    if (!refreshTokens.includes(refreshToken)) {
        return res.sendStatus(403);
    }

    jwt.verify(refreshToken, refreshTokenSecret, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        const accessToken = jwt.sign(
            { username: user.username, role: user.role },
            accessTokenSecret,
            { expiresIn: accessTokenExpireTime }
        );

        res.json({ accessToken });
    });
});

app.post('/logout', (req, res) => {
    const { refreshToken } = req.body;
    refreshTokens = refreshTokens.filter(t => t !== refreshToken);

    res.send('Logout successful!');
});

app.listen(3000, () => {
    console.log('Authentication service started on port 3000!');
});
