const jwt = require('jsonwebtoken');
const { ROLES } = require('./database');
const tokenSecret = require('./env');

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        res.sendStatus(401);
    }

    const token = authHeader.split(' ')[1];

    jwt.verify(token, tokenSecret, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        req.user = user;
        next();
    });
};

/**
 * NOTE: Must come *after* `authenticateJWT` in callstack.
 */
const allowAdminOnly = (req, res, next) => {
    const { role } = req.user;

    if (role !== ROLES.ADMIN) {
        return res.sendStatus(403);
    }

    next();
};

module.exports = {
    authenticateJWT,
    allowAdminOnly,
};
