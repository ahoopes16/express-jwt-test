# README

This repository is meant to be a basic example of how to implement authentication with JWT in an Express/Node.js application.

## How to Run

1. Open up two terminal windows and navigate to this repository
1. In one window, run `npm install` or `yarn`, depending on your preference
1. In one window, run `yarn start-auth` to start the authentication server
1. In the other window, run `yarn start-books` to start the books API server
1. Open Postman and import the environment and the collection

Behold! You can interact with the API. Log in, log out, try changing the JWTs and see what happens. :D

## Author

-   [Alex Hoopes](mailto:alexh@haystackdev.com)

## Resources

This was built with inspiration from this [tutorial](https://stackabuse.com/authentication-and-authorization-with-jwts-in-express-js/) from StackAbuse. Of course, a true implementation of this would require a database connection and encryption/decryption between the application and the server, but this is a good start.

I also recommend pulling up [jwt.io](https://jwt.io/) while testing this out - it's a great tool for debugging and experimenting with JWTs.
